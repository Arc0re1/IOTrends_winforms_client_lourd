﻿using System;
using Newtonsoft.Json;

namespace IOTrends_ClientLourd.Models
{
	// Token: 0x02000005 RID: 5
	public class User
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000011 RID: 17 RVA: 0x00002FF0 File Offset: 0x000011F0
		// (set) Token: 0x06000012 RID: 18 RVA: 0x00002FF8 File Offset: 0x000011F8
		[JsonProperty("id_utilisateur")]
		public string IdUtilisateur
		{
			get;
			set;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000013 RID: 19 RVA: 0x00003001 File Offset: 0x00001201
		// (set) Token: 0x06000014 RID: 20 RVA: 0x00003009 File Offset: 0x00001209
		[JsonProperty("nom")]
		public string Nom
		{
			get;
			set;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000015 RID: 21 RVA: 0x00003012 File Offset: 0x00001212
		// (set) Token: 0x06000016 RID: 22 RVA: 0x0000301A File Offset: 0x0000121A
		[JsonProperty("prenom")]
		public string Prenom
		{
			get;
			set;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000017 RID: 23 RVA: 0x00003023 File Offset: 0x00001223
		// (set) Token: 0x06000018 RID: 24 RVA: 0x0000302B File Offset: 0x0000122B
		[JsonProperty("password")]
		public string Password
		{
			get;
			set;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000019 RID: 25 RVA: 0x00003034 File Offset: 0x00001234
		// (set) Token: 0x0600001A RID: 26 RVA: 0x0000303C File Offset: 0x0000123C
		[JsonProperty("id_droits")]
		public string IdDroits
		{
			get;
			set;
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00003045 File Offset: 0x00001245
		// (set) Token: 0x0600001C RID: 28 RVA: 0x0000304D File Offset: 0x0000124D
		[JsonProperty("str_droits")]
		public string Droits
		{
			get;
			set;
		}
	}
}
