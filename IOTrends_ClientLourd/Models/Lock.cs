﻿using Newtonsoft.Json;

namespace IOTrends_ClientLourd.Models
{
    class Lock
    {
        [JsonProperty("id_verrou")]
        public string Id { get; set; }

        [JsonProperty("lib_verrou")]
        public string Label { get; set; }

        [JsonProperty("id_droits")]
        public string Rights { get; set; }

        [JsonProperty("lib_droits")]
        public string LabelRights { get; set; }
    }
}
