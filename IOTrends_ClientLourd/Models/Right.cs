﻿using Newtonsoft.Json;

namespace IOTrends_ClientLourd.Models
{
    class Right
    {
        [JsonProperty("id_droits")]
        public string IdRight { get; set; }

        [JsonProperty("lib_droits")]
        public string LabelRight { get; set; }
    }
}
