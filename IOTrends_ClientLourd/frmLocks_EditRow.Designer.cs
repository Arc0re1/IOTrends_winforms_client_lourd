﻿namespace IOTrends_ClientLourd.Models
{
    partial class frmLocks_EditRow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLabel = new System.Windows.Forms.Label();
            this.lblRights = new System.Windows.Forms.Label();
            this.lblLockID = new System.Windows.Forms.Label();
            this.txtLockID = new System.Windows.Forms.TextBox();
            this.txtLockLabel = new System.Windows.Forms.TextBox();
            this.cmbLockRights = new System.Windows.Forms.ComboBox();
            this.btnSaveLockForm = new System.Windows.Forms.Button();
            this.btnEditFormCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblLabel
            // 
            this.lblLabel.AutoSize = true;
            this.lblLabel.Location = new System.Drawing.Point(14, 33);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(39, 13);
            this.lblLabel.TabIndex = 0;
            this.lblLabel.Text = "Label :";
            // 
            // lblRights
            // 
            this.lblRights.AutoSize = true;
            this.lblRights.Location = new System.Drawing.Point(12, 57);
            this.lblRights.Name = "lblRights";
            this.lblRights.Size = new System.Drawing.Size(40, 13);
            this.lblRights.TabIndex = 1;
            this.lblRights.Text = "Droits :";
            // 
            // lblLockID
            // 
            this.lblLockID.AutoSize = true;
            this.lblLockID.Location = new System.Drawing.Point(14, 9);
            this.lblLockID.Name = "lblLockID";
            this.lblLockID.Size = new System.Drawing.Size(72, 13);
            this.lblLockID.TabIndex = 2;
            this.lblLockID.Text = "ID du verrou :";
            // 
            // txtLockID
            // 
            this.txtLockID.Location = new System.Drawing.Point(113, 9);
            this.txtLockID.Name = "txtLockID";
            this.txtLockID.ReadOnly = true;
            this.txtLockID.Size = new System.Drawing.Size(100, 20);
            this.txtLockID.TabIndex = 3;
            // 
            // txtLockLabel
            // 
            this.txtLockLabel.Location = new System.Drawing.Point(113, 33);
            this.txtLockLabel.Name = "txtLockLabel";
            this.txtLockLabel.Size = new System.Drawing.Size(100, 20);
            this.txtLockLabel.TabIndex = 4;
            // 
            // cmbLockRights
            // 
            this.cmbLockRights.FormattingEnabled = true;
            this.cmbLockRights.Location = new System.Drawing.Point(113, 57);
            this.cmbLockRights.Name = "cmbLockRights";
            this.cmbLockRights.Size = new System.Drawing.Size(100, 21);
            this.cmbLockRights.TabIndex = 5;
            // 
            // btnSaveLockForm
            // 
            this.btnSaveLockForm.Location = new System.Drawing.Point(54, 238);
            this.btnSaveLockForm.Name = "btnSaveLockForm";
            this.btnSaveLockForm.Size = new System.Drawing.Size(75, 23);
            this.btnSaveLockForm.TabIndex = 6;
            this.btnSaveLockForm.Text = "Enregistrer";
            this.btnSaveLockForm.UseVisualStyleBackColor = true;
            this.btnSaveLockForm.Click += new System.EventHandler(this.btnSaveLockForm_Click);
            // 
            // btnEditFormCancel
            // 
            this.btnEditFormCancel.Location = new System.Drawing.Point(165, 238);
            this.btnEditFormCancel.Name = "btnEditFormCancel";
            this.btnEditFormCancel.Size = new System.Drawing.Size(75, 23);
            this.btnEditFormCancel.TabIndex = 7;
            this.btnEditFormCancel.Text = "Annuler";
            this.btnEditFormCancel.UseVisualStyleBackColor = true;
            this.btnEditFormCancel.Click += new System.EventHandler(this.btnEditFormCancel_Click);
            // 
            // frmLocks_EditRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.btnEditFormCancel);
            this.Controls.Add(this.btnSaveLockForm);
            this.Controls.Add(this.cmbLockRights);
            this.Controls.Add(this.txtLockLabel);
            this.Controls.Add(this.txtLockID);
            this.Controls.Add(this.lblLockID);
            this.Controls.Add(this.lblRights);
            this.Controls.Add(this.lblLabel);
            this.Name = "frmLocks_EditRow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLocks_EditRow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLabel;
        private System.Windows.Forms.Label lblRights;
        private System.Windows.Forms.Label lblLockID;
        private System.Windows.Forms.TextBox txtLockID;
        private System.Windows.Forms.TextBox txtLockLabel;
        private System.Windows.Forms.ComboBox cmbLockRights;
        private System.Windows.Forms.Button btnSaveLockForm;
        private System.Windows.Forms.Button btnEditFormCancel;
    }
}