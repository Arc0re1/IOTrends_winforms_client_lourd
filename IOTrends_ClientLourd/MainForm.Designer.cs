﻿namespace IOTrends_ClientLourd
{
	// Token: 0x02000004 RID: 4
	public partial class MainForm : global::System.Windows.Forms.Form
	{
		// Token: 0x0600000E RID: 14 RVA: 0x000026E8 File Offset: 0x000008E8
		[global::System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				bool flag = disposing && this.components != null;
				if (flag)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002738 File Offset: 0x00000938
		[global::System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.txtAdmRights = new System.Windows.Forms.TextBox();
            this.txtAdmFirstName = new System.Windows.Forms.TextBox();
            this.txtAdmPassword = new System.Windows.Forms.TextBox();
            this.txtAdmDatabaseID = new System.Windows.Forms.TextBox();
            this.txtAdmName = new System.Windows.Forms.TextBox();
            this.lblAdmRights = new System.Windows.Forms.Label();
            this.lblAdmDatabaseID = new System.Windows.Forms.Label();
            this.lblAdmPassword = new System.Windows.Forms.Label();
            this.lblAdmFirstName = new System.Windows.Forms.Label();
            this.lblAdmName = new System.Windows.Forms.Label();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.btnAddNewLock = new System.Windows.Forms.Button();
            this.lblVerrRefreshLoading = new System.Windows.Forms.Label();
            this.grdLocksList = new System.Windows.Forms.DataGridView();
            this.btnVerrRefresh = new System.Windows.Forms.Button();
            this.btnInfoAdmModify = new System.Windows.Forms.Button();
            this.btnAdmReload = new System.Windows.Forms.Button();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            this.TabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLocksList)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Location = new System.Drawing.Point(-2, -1);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.ShowToolTips = true;
            this.TabControl1.Size = new System.Drawing.Size(788, 561);
            this.TabControl1.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.btnAdmReload);
            this.TabPage1.Controls.Add(this.btnInfoAdmModify);
            this.TabPage1.Controls.Add(this.txtAdmRights);
            this.TabPage1.Controls.Add(this.txtAdmFirstName);
            this.TabPage1.Controls.Add(this.txtAdmPassword);
            this.TabPage1.Controls.Add(this.txtAdmDatabaseID);
            this.TabPage1.Controls.Add(this.txtAdmName);
            this.TabPage1.Controls.Add(this.lblAdmRights);
            this.TabPage1.Controls.Add(this.lblAdmDatabaseID);
            this.TabPage1.Controls.Add(this.lblAdmPassword);
            this.TabPage1.Controls.Add(this.lblAdmFirstName);
            this.TabPage1.Controls.Add(this.lblAdmName);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(780, 535);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "InfoAdm";
            this.TabPage1.ToolTipText = "Informations concernant l\'administrateur";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // txtAdmRights
            // 
            this.txtAdmRights.Location = new System.Drawing.Point(149, 159);
            this.txtAdmRights.Name = "txtAdmRights";
            this.txtAdmRights.ReadOnly = true;
            this.txtAdmRights.Size = new System.Drawing.Size(121, 20);
            this.txtAdmRights.TabIndex = 9;
            // 
            // txtAdmFirstName
            // 
            this.txtAdmFirstName.Location = new System.Drawing.Point(149, 72);
            this.txtAdmFirstName.Name = "txtAdmFirstName";
            this.txtAdmFirstName.ReadOnly = true;
            this.txtAdmFirstName.Size = new System.Drawing.Size(121, 20);
            this.txtAdmFirstName.TabIndex = 8;
            // 
            // txtAdmPassword
            // 
            this.txtAdmPassword.Location = new System.Drawing.Point(149, 99);
            this.txtAdmPassword.Name = "txtAdmPassword";
            this.txtAdmPassword.ReadOnly = true;
            this.txtAdmPassword.Size = new System.Drawing.Size(121, 20);
            this.txtAdmPassword.TabIndex = 7;
            this.txtAdmPassword.UseSystemPasswordChar = true;
            // 
            // txtAdmDatabaseID
            // 
            this.txtAdmDatabaseID.Location = new System.Drawing.Point(149, 128);
            this.txtAdmDatabaseID.Name = "txtAdmDatabaseID";
            this.txtAdmDatabaseID.ReadOnly = true;
            this.txtAdmDatabaseID.Size = new System.Drawing.Size(121, 20);
            this.txtAdmDatabaseID.TabIndex = 6;
            // 
            // txtAdmName
            // 
            this.txtAdmName.Location = new System.Drawing.Point(149, 42);
            this.txtAdmName.Name = "txtAdmName";
            this.txtAdmName.ReadOnly = true;
            this.txtAdmName.Size = new System.Drawing.Size(121, 20);
            this.txtAdmName.TabIndex = 5;
            // 
            // lblAdmRights
            // 
            this.lblAdmRights.AutoSize = true;
            this.lblAdmRights.Location = new System.Drawing.Point(28, 159);
            this.lblAdmRights.Name = "lblAdmRights";
            this.lblAdmRights.Size = new System.Drawing.Size(40, 13);
            this.lblAdmRights.TabIndex = 4;
            this.lblAdmRights.Text = "Droits :";
            // 
            // lblAdmDatabaseID
            // 
            this.lblAdmDatabaseID.AutoSize = true;
            this.lblAdmDatabaseID.Location = new System.Drawing.Point(28, 128);
            this.lblAdmDatabaseID.Name = "lblAdmDatabaseID";
            this.lblAdmDatabaseID.Size = new System.Drawing.Size(65, 13);
            this.lblAdmDatabaseID.TabIndex = 3;
            this.lblAdmDatabaseID.Text = "ID en base :";
            // 
            // lblAdmPassword
            // 
            this.lblAdmPassword.AutoSize = true;
            this.lblAdmPassword.Location = new System.Drawing.Point(28, 99);
            this.lblAdmPassword.Name = "lblAdmPassword";
            this.lblAdmPassword.Size = new System.Drawing.Size(77, 13);
            this.lblAdmPassword.TabIndex = 2;
            this.lblAdmPassword.Text = "Mot de passe :";
            // 
            // lblAdmFirstName
            // 
            this.lblAdmFirstName.AutoSize = true;
            this.lblAdmFirstName.Location = new System.Drawing.Point(28, 72);
            this.lblAdmFirstName.Name = "lblAdmFirstName";
            this.lblAdmFirstName.Size = new System.Drawing.Size(49, 13);
            this.lblAdmFirstName.TabIndex = 1;
            this.lblAdmFirstName.Text = "Prénom :";
            // 
            // lblAdmName
            // 
            this.lblAdmName.AutoSize = true;
            this.lblAdmName.Location = new System.Drawing.Point(28, 42);
            this.lblAdmName.Name = "lblAdmName";
            this.lblAdmName.Size = new System.Drawing.Size(35, 13);
            this.lblAdmName.TabIndex = 0;
            this.lblAdmName.Text = "Nom :";
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.btnAddNewLock);
            this.TabPage2.Controls.Add(this.lblVerrRefreshLoading);
            this.TabPage2.Controls.Add(this.grdLocksList);
            this.TabPage2.Controls.Add(this.btnVerrRefresh);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(780, 535);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "Verr";
            this.TabPage2.ToolTipText = "Récapitulatif des verrous";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // btnAddNewLock
            // 
            this.btnAddNewLock.Location = new System.Drawing.Point(113, 496);
            this.btnAddNewLock.Name = "btnAddNewLock";
            this.btnAddNewLock.Size = new System.Drawing.Size(102, 23);
            this.btnAddNewLock.TabIndex = 3;
            this.btnAddNewLock.Text = "Ajouter un verrou";
            this.btnAddNewLock.UseVisualStyleBackColor = true;
            this.btnAddNewLock.Click += new System.EventHandler(this.btnAddNewLock_Click);
            // 
            // lblVerrRefreshLoading
            // 
            this.lblVerrRefreshLoading.AutoSize = true;
            this.lblVerrRefreshLoading.Location = new System.Drawing.Point(110, 505);
            this.lblVerrRefreshLoading.Name = "lblVerrRefreshLoading";
            this.lblVerrRefreshLoading.Size = new System.Drawing.Size(0, 13);
            this.lblVerrRefreshLoading.TabIndex = 2;
            // 
            // grdLocksList
            // 
            this.grdLocksList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdLocksList.Location = new System.Drawing.Point(11, 6);
            this.grdLocksList.Name = "grdLocksList";
            this.grdLocksList.Size = new System.Drawing.Size(759, 484);
            this.grdLocksList.TabIndex = 1;
            // 
            // btnVerrRefresh
            // 
            this.btnVerrRefresh.Location = new System.Drawing.Point(11, 496);
            this.btnVerrRefresh.Name = "btnVerrRefresh";
            this.btnVerrRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnVerrRefresh.TabIndex = 0;
            this.btnVerrRefresh.Text = "Recharger";
            this.btnVerrRefresh.UseVisualStyleBackColor = true;
            this.btnVerrRefresh.Click += new System.EventHandler(this.btnVerrRefresh_Click);
            // 
            // btnInfoAdmModify
            // 
            this.btnInfoAdmModify.Location = new System.Drawing.Point(31, 481);
            this.btnInfoAdmModify.Name = "btnInfoAdmModify";
            this.btnInfoAdmModify.Size = new System.Drawing.Size(75, 23);
            this.btnInfoAdmModify.TabIndex = 10;
            this.btnInfoAdmModify.Text = "Modifier ...";
            this.btnInfoAdmModify.UseVisualStyleBackColor = true;
            this.btnInfoAdmModify.Click += new System.EventHandler(this.btnInfoAdmModify_Click);
            // 
            // btnAdmReload
            // 
            this.btnAdmReload.Location = new System.Drawing.Point(149, 480);
            this.btnAdmReload.Name = "btnAdmReload";
            this.btnAdmReload.Size = new System.Drawing.Size(75, 23);
            this.btnAdmReload.TabIndex = 11;
            this.btnAdmReload.Text = "Recharger";
            this.btnAdmReload.UseVisualStyleBackColor = true;
            this.btnAdmReload.Click += new System.EventHandler(this.btnAdmReload_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.TabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLocksList)).EndInit();
            this.ResumeLayout(false);

		}

		// Token: 0x0400000B RID: 11
		private global::System.ComponentModel.IContainer components;

		// Token: 0x0400000C RID: 12
		internal global::System.Windows.Forms.TabControl TabControl1;

		// Token: 0x0400000D RID: 13
		internal global::System.Windows.Forms.TabPage TabPage1;

		// Token: 0x0400000E RID: 14
		internal global::System.Windows.Forms.TabPage TabPage2;

		// Token: 0x0400000F RID: 15
		internal global::System.Windows.Forms.Label lblAdmRights;

		// Token: 0x04000010 RID: 16
		internal global::System.Windows.Forms.Label lblAdmDatabaseID;

		// Token: 0x04000011 RID: 17
		internal global::System.Windows.Forms.Label lblAdmPassword;

		// Token: 0x04000012 RID: 18
		internal global::System.Windows.Forms.Label lblAdmFirstName;

		// Token: 0x04000013 RID: 19
		internal global::System.Windows.Forms.Label lblAdmName;

		// Token: 0x04000014 RID: 20
		internal global::System.Windows.Forms.TextBox txtAdmFirstName;

		// Token: 0x04000015 RID: 21
		internal global::System.Windows.Forms.TextBox txtAdmPassword;

		// Token: 0x04000016 RID: 22
		internal global::System.Windows.Forms.TextBox txtAdmDatabaseID;

		// Token: 0x04000017 RID: 23
		internal global::System.Windows.Forms.TextBox txtAdmName;

		// Token: 0x04000018 RID: 24
		internal global::System.Windows.Forms.TextBox txtAdmRights;
        private System.Windows.Forms.Button btnVerrRefresh;
        private System.Windows.Forms.DataGridView grdLocksList;
        private System.Windows.Forms.Label lblVerrRefreshLoading;
        private System.Windows.Forms.Button btnAddNewLock;
        private System.Windows.Forms.Button btnInfoAdmModify;
        private System.Windows.Forms.Button btnAdmReload;
    }
}
