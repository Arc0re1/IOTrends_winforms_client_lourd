﻿namespace IOTrends_ClientLourd
{
    partial class frmLocks_AddRow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEditFormCancel = new System.Windows.Forms.Button();
            this.btnSaveLockForm = new System.Windows.Forms.Button();
            this.cmbLockRights = new System.Windows.Forms.ComboBox();
            this.txtLockLabel = new System.Windows.Forms.TextBox();
            this.lblRights = new System.Windows.Forms.Label();
            this.lblLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEditFormCancel
            // 
            this.btnEditFormCancel.Location = new System.Drawing.Point(168, 239);
            this.btnEditFormCancel.Name = "btnEditFormCancel";
            this.btnEditFormCancel.Size = new System.Drawing.Size(75, 23);
            this.btnEditFormCancel.TabIndex = 15;
            this.btnEditFormCancel.Text = "Annuler";
            this.btnEditFormCancel.UseVisualStyleBackColor = true;
            this.btnEditFormCancel.Click += new System.EventHandler(this.btnEditFormCancel_Click);
            // 
            // btnSaveLockForm
            // 
            this.btnSaveLockForm.Location = new System.Drawing.Point(48, 239);
            this.btnSaveLockForm.Name = "btnSaveLockForm";
            this.btnSaveLockForm.Size = new System.Drawing.Size(75, 23);
            this.btnSaveLockForm.TabIndex = 14;
            this.btnSaveLockForm.Text = "Enregistrer";
            this.btnSaveLockForm.UseVisualStyleBackColor = true;
            this.btnSaveLockForm.Click += new System.EventHandler(this.btnSaveLockForm_Click);
            // 
            // cmbLockRights
            // 
            this.cmbLockRights.FormattingEnabled = true;
            this.cmbLockRights.Location = new System.Drawing.Point(133, 44);
            this.cmbLockRights.Name = "cmbLockRights";
            this.cmbLockRights.Size = new System.Drawing.Size(100, 21);
            this.cmbLockRights.TabIndex = 13;
            // 
            // txtLockLabel
            // 
            this.txtLockLabel.Location = new System.Drawing.Point(133, 18);
            this.txtLockLabel.Name = "txtLockLabel";
            this.txtLockLabel.Size = new System.Drawing.Size(100, 20);
            this.txtLockLabel.TabIndex = 12;
            // 
            // lblRights
            // 
            this.lblRights.AutoSize = true;
            this.lblRights.Location = new System.Drawing.Point(32, 52);
            this.lblRights.Name = "lblRights";
            this.lblRights.Size = new System.Drawing.Size(40, 13);
            this.lblRights.TabIndex = 9;
            this.lblRights.Text = "Droits :";
            // 
            // lblLabel
            // 
            this.lblLabel.AutoSize = true;
            this.lblLabel.Location = new System.Drawing.Point(33, 18);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(39, 13);
            this.lblLabel.TabIndex = 8;
            this.lblLabel.Text = "Label :";
            // 
            // frmLocks_AddRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.btnEditFormCancel);
            this.Controls.Add(this.btnSaveLockForm);
            this.Controls.Add(this.cmbLockRights);
            this.Controls.Add(this.txtLockLabel);
            this.Controls.Add(this.lblRights);
            this.Controls.Add(this.lblLabel);
            this.Name = "frmLocks_AddRow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLocks_AddRow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEditFormCancel;
        private System.Windows.Forms.Button btnSaveLockForm;
        private System.Windows.Forms.ComboBox cmbLockRights;
        private System.Windows.Forms.TextBox txtLockLabel;
        private System.Windows.Forms.Label lblRights;
        private System.Windows.Forms.Label lblLabel;
    }
}