﻿using System;

namespace IOTrends_ClientLourd
{
	// Token: 0x02000006 RID: 6
	internal static class Strings
	{
		// Token: 0x04000020 RID: 32
		public static string Title_ErrorString = "IOTrends Client - Erreur";

		// Token: 0x04000021 RID: 33
		public static string Title_MainForm = "IOTrends Client - Administration";

		// Token: 0x04000022 RID: 34
		public static string Title_LoginForm = "IOTrends Client - Login";

        public static string Title_Locks_EditRow = "IOTrends Client - Formulaire d'édition";

        public static string Title_Locks_AddRow = "IOTrends Client - Ajout d'un verrou";

        public static string Title_Adm_EditUser = "IOTrends Client - Editer vos informations";

        public static string Verr_RefreshLoadingMessage = "Chargement en cours . . .";

        public static string Verr_Grid_label = "Libellé";

        public static string Verr_Grid_rights = "Droits (appartenance) : Id";

        public static string Verr_Grid_rightsLabel = "Droits (appartenance) : Libellé";

        public static string Verr_Grid_rightClickMenu_delete = "Supprimer ce verrou";

        public static string Popup_Grid_rightClickMenu_delete = "Supprimer ce verrou ?";

        public static string Popup_Login_Failed = "IOTrends Client - Connexion échouée";
    }
}
