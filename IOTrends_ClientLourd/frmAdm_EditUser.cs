﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IOTrends_ClientLourd.Models;

namespace IOTrends_ClientLourd
{
    public partial class frmAdm_EditUser : Form
    {
        private string dbID;
        public frmAdm_EditUser(string dbID)
        {
            this.dbID = dbID;
            InitializeComponent();
            this.PrepareForm();
        }

        private void PrepareForm()
        {
            this.Text = Strings.Title_Adm_EditUser;
            this.txtAdmDatabaseID.Text = this.dbID;
            string rawRights = HttpHelper.GetData("?GetRightsList=1");
            List<Models.Right> rights = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.Right>>(rawRights);
            this.cmbLockRights.DataSource = rights;
            this.cmbLockRights.ValueMember = "IdRight";
            this.cmbLockRights.DisplayMember = "LabelRight";
        }

        private void SaveForm()
        {
            Right newRight = (Right)this.cmbLockRights.SelectedItem;
            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
            json["RequestName"] = "EditUserData";
            json["Name"] = this.txtAdmName.Text;
            json["FirstName"] = this.txtAdmFirstName.Text;
            json["Password"] = this.txtAdmPassword.Text;
            json["Right"] = newRight.IdRight;
            json["UserID"] = this.dbID;
            string jsonToPost = json.ToString();
            HttpHelper.PostData(jsonToPost);
        }

        private void btnAdm_EditUser_Save_Click(object sender, EventArgs e)
        {
            this.SaveForm();
            this.Close();
        }

        private void btnAdm_EditUser_Quit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
