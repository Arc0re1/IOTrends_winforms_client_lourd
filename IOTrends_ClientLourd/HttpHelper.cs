﻿using System;
using System.IO;
using System.Net;

namespace IOTrends_ClientLourd
{
	// Token: 0x02000002 RID: 2
	internal static class HttpHelper
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		public static string GetData(string Params)
		{
			WebRequest request = WebRequest.Create(HttpHelper.ServerUrl + HttpHelper.AdminAPIPage + Params);
			request.Method = "GET";
			WebResponse Response = request.GetResponse();
			Stream Data = Response.GetResponseStream();
			StreamReader reader = new StreamReader(Data);
			string responseFromServer = reader.ReadToEnd();
			Response.Close();
			return responseFromServer;
		}

        public static void PostData(string jsonData)
        {
            if (string.IsNullOrWhiteSpace(jsonData))
            {
                throw new ArgumentNullException();
            }
            string url = HttpHelper.ServerUrl + HttpHelper.AdminAPIPage;
            //string result = "";
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                /*result = */client.UploadString(url, "POST", jsonData);
            }
            //System.Windows.Forms.MessageBox.Show(result);
        }

		// Token: 0x04000001 RID: 1
		private static string ServerUrl = "https://iotrendsss.000webhostapp.com/";

		// Token: 0x04000002 RID: 2
		private static string AdminAPIPage = "ApiAdmin.php";
	}
}
