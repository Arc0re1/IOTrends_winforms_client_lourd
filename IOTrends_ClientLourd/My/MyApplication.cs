﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace IOTrends_ClientLourd.My
{
	// Token: 0x02000007 RID: 7
	internal class MyApplication : WindowsFormsApplicationBase
	{
		// Token: 0x0600001F RID: 31 RVA: 0x0000307F File Offset: 0x0000127F
		[DebuggerStepThrough]
		public MyApplication() : base(AuthenticationMode.Windows)
		{
			base.IsSingleInstance = false;
			base.EnableVisualStyles = true;
			base.SaveMySettingsOnExit = true;
			base.ShutdownStyle = ShutdownMode.AfterMainFormCloses;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000030AA File Offset: 0x000012AA
		[DebuggerStepThrough]
		protected override void OnCreateMainForm()
		{
			base.MainForm = MyProject.Forms.frmLogin;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000030BE File Offset: 0x000012BE
		[STAThread]
		public static void Main(string[] args)
		{
			Application.SetCompatibleTextRenderingDefault(WindowsFormsApplicationBase.UseCompatibleTextRendering);
			MyProject.Application.Run(args);
		}
	}
}
