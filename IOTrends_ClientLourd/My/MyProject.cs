﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace IOTrends_ClientLourd.My
{
	// Token: 0x0200000A RID: 10
	internal sealed class MyProject
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000031C0 File Offset: 0x000013C0
		public static MyApplication Application
		{
			[DebuggerStepThrough]
			get
			{
				bool flag = MyProject.application == null;
				if (flag)
				{
					MyProject.application = new MyApplication();
				}
				return MyProject.application;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000028 RID: 40 RVA: 0x000031F0 File Offset: 0x000013F0
		public static MyComputer Computer
		{
			[DebuggerStepThrough]
			get
			{
				bool flag = MyProject.computer == null;
				if (flag)
				{
					MyProject.computer = new MyComputer();
				}
				return MyProject.computer;
			}
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000029 RID: 41 RVA: 0x00003220 File Offset: 0x00001420
		public static global::IOTrends_ClientLourd.Models.User User
		{
			[DebuggerStepThrough]
			get
			{
				bool flag = MyProject.user == null;
				if (flag)
				{
					MyProject.user = new global::IOTrends_ClientLourd.Models.User();
				}
				return MyProject.user;
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600002A RID: 42 RVA: 0x00003250 File Offset: 0x00001450
		public static MyProject.MyForms Forms
		{
			[DebuggerStepThrough]
			get
			{
				bool flag = MyProject.forms == null;
				if (flag)
				{
					MyProject.forms = new MyProject.MyForms();
				}
				return MyProject.forms;
			}
		}

		// Token: 0x04000026 RID: 38
		[ThreadStatic]
		private static MyApplication application;

		// Token: 0x04000027 RID: 39
		[ThreadStatic]
		private static MyComputer computer;

		// Token: 0x04000028 RID: 40
		[ThreadStatic]
		private static global::IOTrends_ClientLourd.Models.User user;

		// Token: 0x04000029 RID: 41
		[ThreadStatic]
		private static MyProject.MyForms forms;

		// Token: 0x0200000D RID: 13
		internal sealed class MyForms
		{
			// Token: 0x17000011 RID: 17
			// (get) Token: 0x06000030 RID: 48 RVA: 0x000032F0 File Offset: 0x000014F0
			// (set) Token: 0x06000031 RID: 49 RVA: 0x00003313 File Offset: 0x00001513
			public MainForm MainForm
			{
				[DebuggerStepThrough]
				get
				{
					return MyProject.MyForms.GetForm<MainForm>(ref this.MainForm_instance, ref this.MainForm_isCreating);
				}
				[DebuggerStepThrough]
				set
				{
					MyProject.MyForms.SetForm<MainForm>(ref this.MainForm_instance, value);
				}
			}

			// Token: 0x17000012 RID: 18
			// (get) Token: 0x06000032 RID: 50 RVA: 0x00003324 File Offset: 0x00001524
			// (set) Token: 0x06000033 RID: 51 RVA: 0x00003347 File Offset: 0x00001547
			public frmLogin frmLogin
			{
				[DebuggerStepThrough]
				get
				{
					return MyProject.MyForms.GetForm<frmLogin>(ref this.frmLogin_instance, ref this.frmLogin_isCreating);
				}
				[DebuggerStepThrough]
				set
				{
					MyProject.MyForms.SetForm<frmLogin>(ref this.frmLogin_instance, value);
				}
			}

			// Token: 0x06000034 RID: 52 RVA: 0x00003358 File Offset: 0x00001558
			[DebuggerStepThrough]
			private static T GetForm<T>(ref T instance, ref bool isCreating) where T : Form, new()
			{
				bool flag = instance == null || instance.IsDisposed;
				if (flag)
				{
					bool flag2 = isCreating;
					if (flag2)
					{
						throw new InvalidOperationException(Utils.GetResourceString("WinForms_RecursiveFormCreate", new string[0]));
					}
					isCreating = true;
					try
					{
						instance = Activator.CreateInstance<T>();
					}
					catch (TargetInvocationException ex)
					{
						throw new InvalidOperationException(Utils.GetResourceString("WinForms_SeeInnerException", new string[]
						{
							ex.InnerException.Message
						}), ex.InnerException);
					}
					finally
					{
						isCreating = false;
					}
				}
				return instance;
			}

			// Token: 0x06000035 RID: 53 RVA: 0x00003414 File Offset: 0x00001614
			[DebuggerStepThrough]
			private static void SetForm<T>(ref T instance, T value) where T : Form
			{
				bool flag = instance != value;
				if (flag)
				{
					bool flag2 = value == null;
					if (!flag2)
					{
						throw new ArgumentException("Property can only be set to null");
					}
					instance.Dispose();
					instance = default(T);
				}
			}

			// Token: 0x0400002C RID: 44
			private MainForm MainForm_instance;

			// Token: 0x0400002D RID: 45
			private bool MainForm_isCreating;

			// Token: 0x0400002E RID: 46
			private frmLogin frmLogin_instance;

			// Token: 0x0400002F RID: 47
			private bool frmLogin_isCreating;
		}
	}
}
