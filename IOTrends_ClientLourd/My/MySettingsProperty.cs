﻿using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic;

namespace IOTrends_ClientLourd.My
{
	// Token: 0x02000009 RID: 9
	[HideModuleName, DebuggerNonUserCode, CompilerGenerated]
	internal static class MySettingsProperty
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000026 RID: 38 RVA: 0x000031A8 File Offset: 0x000013A8
		[HelpKeyword("My.Settings")]
		internal static MySettings Settings
		{
			get
			{
				return MySettings.Default;
			}
		}
	}
}
