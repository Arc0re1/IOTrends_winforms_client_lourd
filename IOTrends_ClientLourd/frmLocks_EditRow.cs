﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace IOTrends_ClientLourd.Models
{
    public partial class frmLocks_EditRow : Form
    {
        private DataGridViewRow currentRow;

        public frmLocks_EditRow(ref DataGridView formGrid, DataGridViewRow currentRow)
        {
            this.currentRow = currentRow;
            InitializeComponent();
            this.Text = Strings.Title_Locks_EditRow;
            this.FillUpForm();
        }

        private void FillUpForm()
        {
            this.txtLockID.Text = this.currentRow.Cells["Id"].Value.ToString();
            this.txtLockLabel.Text = this.currentRow.Cells["Label"].Value.ToString();
            string rawRights = HttpHelper.GetData("?GetRightsList=1");
            List<Right> rights = JsonConvert.DeserializeObject<List<Right>>(rawRights);
            this.cmbLockRights.DataSource = rights;
            this.cmbLockRights.ValueMember = "IdRight";
            this.cmbLockRights.DisplayMember = "LabelRight";
            this.cmbLockRights.SelectedItem = rights.Find(item => item.IdRight == this.currentRow.Cells["Rights"].Value.ToString());
        }

        private void SaveForm()
        {
            Right newRight = (Right)this.cmbLockRights.SelectedItem;
            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
            json["RequestName"] = "SaveLockData";
            json["LockID"] = this.txtLockID.Text;
            json["Label"] = this.txtLockLabel.Text;
            json["Right"] = newRight.IdRight;
            string jsonToPost = json.ToString();
            HttpHelper.PostData(jsonToPost);
            this.Close();
        }

        private void btnEditFormCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSaveLockForm_Click(object sender, EventArgs e)
        {
            this.SaveForm();
        }
    }
}
