﻿using IOTrends_ClientLourd.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IOTrends_ClientLourd
{
    public partial class frmLocks_AddRow : Form
    {
        public frmLocks_AddRow(ref DataGridView formGrid)
        {
            InitializeComponent();
            this.PrepareForm();
        }

        private void PrepareForm()
        {
            this.Text = Strings.Title_Locks_AddRow;
            string rawRights = HttpHelper.GetData("?GetRightsList=1");
            List<Right> rights = JsonConvert.DeserializeObject<List<Right>>(rawRights);
            this.cmbLockRights.DataSource = rights;
            this.cmbLockRights.ValueMember = "IdRight";
            this.cmbLockRights.DisplayMember = "LabelRight";
        }

        private void SaveForm()
        {
            Right newRight = (Right)this.cmbLockRights.SelectedItem;
            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
            json["RequestName"] = "SaveNewLockData";
            json["Label"] = this.txtLockLabel.Text;
            json["Right"] = newRight.IdRight;
            string jsonToPost = json.ToString();
            HttpHelper.PostData(jsonToPost);
            this.Close();
        }

        private void btnSaveLockForm_Click(object sender, EventArgs e)
        {
            this.SaveForm();
            this.Close();
        }

        private void btnEditFormCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
