﻿namespace IOTrends_ClientLourd
{
    partial class frmAdm_EditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAdmFirstName = new System.Windows.Forms.TextBox();
            this.txtAdmPassword = new System.Windows.Forms.TextBox();
            this.txtAdmDatabaseID = new System.Windows.Forms.TextBox();
            this.txtAdmName = new System.Windows.Forms.TextBox();
            this.lblAdmRights = new System.Windows.Forms.Label();
            this.lblAdmDatabaseID = new System.Windows.Forms.Label();
            this.lblAdmPassword = new System.Windows.Forms.Label();
            this.lblAdmFirstName = new System.Windows.Forms.Label();
            this.lblAdmName = new System.Windows.Forms.Label();
            this.btnAdm_EditUser_Save = new System.Windows.Forms.Button();
            this.btnAdm_EditUser_Quit = new System.Windows.Forms.Button();
            this.cmbLockRights = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtAdmFirstName
            // 
            this.txtAdmFirstName.Location = new System.Drawing.Point(138, 42);
            this.txtAdmFirstName.Name = "txtAdmFirstName";
            this.txtAdmFirstName.Size = new System.Drawing.Size(121, 20);
            this.txtAdmFirstName.TabIndex = 18;
            // 
            // txtAdmPassword
            // 
            this.txtAdmPassword.Location = new System.Drawing.Point(138, 69);
            this.txtAdmPassword.Name = "txtAdmPassword";
            this.txtAdmPassword.Size = new System.Drawing.Size(121, 20);
            this.txtAdmPassword.TabIndex = 17;
            this.txtAdmPassword.UseSystemPasswordChar = true;
            // 
            // txtAdmDatabaseID
            // 
            this.txtAdmDatabaseID.Location = new System.Drawing.Point(138, 98);
            this.txtAdmDatabaseID.Name = "txtAdmDatabaseID";
            this.txtAdmDatabaseID.ReadOnly = true;
            this.txtAdmDatabaseID.Size = new System.Drawing.Size(121, 20);
            this.txtAdmDatabaseID.TabIndex = 16;
            // 
            // txtAdmName
            // 
            this.txtAdmName.Location = new System.Drawing.Point(138, 12);
            this.txtAdmName.Name = "txtAdmName";
            this.txtAdmName.Size = new System.Drawing.Size(121, 20);
            this.txtAdmName.TabIndex = 15;
            // 
            // lblAdmRights
            // 
            this.lblAdmRights.AutoSize = true;
            this.lblAdmRights.Location = new System.Drawing.Point(17, 129);
            this.lblAdmRights.Name = "lblAdmRights";
            this.lblAdmRights.Size = new System.Drawing.Size(40, 13);
            this.lblAdmRights.TabIndex = 14;
            this.lblAdmRights.Text = "Droits :";
            // 
            // lblAdmDatabaseID
            // 
            this.lblAdmDatabaseID.AutoSize = true;
            this.lblAdmDatabaseID.Location = new System.Drawing.Point(17, 98);
            this.lblAdmDatabaseID.Name = "lblAdmDatabaseID";
            this.lblAdmDatabaseID.Size = new System.Drawing.Size(65, 13);
            this.lblAdmDatabaseID.TabIndex = 13;
            this.lblAdmDatabaseID.Text = "ID en base :";
            // 
            // lblAdmPassword
            // 
            this.lblAdmPassword.AutoSize = true;
            this.lblAdmPassword.Location = new System.Drawing.Point(17, 69);
            this.lblAdmPassword.Name = "lblAdmPassword";
            this.lblAdmPassword.Size = new System.Drawing.Size(77, 13);
            this.lblAdmPassword.TabIndex = 12;
            this.lblAdmPassword.Text = "Mot de passe :";
            // 
            // lblAdmFirstName
            // 
            this.lblAdmFirstName.AutoSize = true;
            this.lblAdmFirstName.Location = new System.Drawing.Point(17, 42);
            this.lblAdmFirstName.Name = "lblAdmFirstName";
            this.lblAdmFirstName.Size = new System.Drawing.Size(49, 13);
            this.lblAdmFirstName.TabIndex = 11;
            this.lblAdmFirstName.Text = "Prénom :";
            // 
            // lblAdmName
            // 
            this.lblAdmName.AutoSize = true;
            this.lblAdmName.Location = new System.Drawing.Point(17, 12);
            this.lblAdmName.Name = "lblAdmName";
            this.lblAdmName.Size = new System.Drawing.Size(35, 13);
            this.lblAdmName.TabIndex = 10;
            this.lblAdmName.Text = "Nom :";
            // 
            // btnAdm_EditUser_Save
            // 
            this.btnAdm_EditUser_Save.Location = new System.Drawing.Point(58, 203);
            this.btnAdm_EditUser_Save.Name = "btnAdm_EditUser_Save";
            this.btnAdm_EditUser_Save.Size = new System.Drawing.Size(78, 28);
            this.btnAdm_EditUser_Save.TabIndex = 20;
            this.btnAdm_EditUser_Save.Text = "Enregistrer";
            this.btnAdm_EditUser_Save.UseVisualStyleBackColor = true;
            this.btnAdm_EditUser_Save.Click += new System.EventHandler(this.btnAdm_EditUser_Save_Click);
            // 
            // btnAdm_EditUser_Quit
            // 
            this.btnAdm_EditUser_Quit.Location = new System.Drawing.Point(157, 203);
            this.btnAdm_EditUser_Quit.Name = "btnAdm_EditUser_Quit";
            this.btnAdm_EditUser_Quit.Size = new System.Drawing.Size(78, 28);
            this.btnAdm_EditUser_Quit.TabIndex = 21;
            this.btnAdm_EditUser_Quit.Text = "Annuler";
            this.btnAdm_EditUser_Quit.UseVisualStyleBackColor = true;
            this.btnAdm_EditUser_Quit.Click += new System.EventHandler(this.btnAdm_EditUser_Quit_Click);
            // 
            // cmbLockRights
            // 
            this.cmbLockRights.FormattingEnabled = true;
            this.cmbLockRights.Location = new System.Drawing.Point(138, 126);
            this.cmbLockRights.Name = "cmbLockRights";
            this.cmbLockRights.Size = new System.Drawing.Size(121, 21);
            this.cmbLockRights.TabIndex = 22;
            // 
            // frmAdm_EditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.cmbLockRights);
            this.Controls.Add(this.btnAdm_EditUser_Quit);
            this.Controls.Add(this.btnAdm_EditUser_Save);
            this.Controls.Add(this.txtAdmFirstName);
            this.Controls.Add(this.txtAdmPassword);
            this.Controls.Add(this.txtAdmDatabaseID);
            this.Controls.Add(this.txtAdmName);
            this.Controls.Add(this.lblAdmRights);
            this.Controls.Add(this.lblAdmDatabaseID);
            this.Controls.Add(this.lblAdmPassword);
            this.Controls.Add(this.lblAdmFirstName);
            this.Controls.Add(this.lblAdmName);
            this.Name = "frmAdm_EditUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAdm_EditUser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.TextBox txtAdmFirstName;
        internal System.Windows.Forms.TextBox txtAdmPassword;
        internal System.Windows.Forms.TextBox txtAdmDatabaseID;
        internal System.Windows.Forms.TextBox txtAdmName;
        internal System.Windows.Forms.Label lblAdmRights;
        internal System.Windows.Forms.Label lblAdmDatabaseID;
        internal System.Windows.Forms.Label lblAdmPassword;
        internal System.Windows.Forms.Label lblAdmFirstName;
        internal System.Windows.Forms.Label lblAdmName;
        private System.Windows.Forms.Button btnAdm_EditUser_Save;
        private System.Windows.Forms.Button btnAdm_EditUser_Quit;
        private System.Windows.Forms.ComboBox cmbLockRights;
    }
}