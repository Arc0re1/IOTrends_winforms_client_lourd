﻿namespace IOTrends_ClientLourd
{
	// Token: 0x02000003 RID: 3
	public partial class frmLogin : global::System.Windows.Forms.Form
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002190 File Offset: 0x00000390
		[global::System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
			{
				bool flag = disposing && this.components != null;
				if (flag)
				{
					this.components.Dispose();
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x06000006 RID: 6 RVA: 0x000021E0 File Offset: 0x000003E0
		[global::System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Label1 = new global::System.Windows.Forms.Label();
			this.lblLogin = new global::System.Windows.Forms.Label();
			this.txtLogin = new global::System.Windows.Forms.TextBox();
			this.lblPassword = new global::System.Windows.Forms.Label();
			this.txtPassword = new global::System.Windows.Forms.TextBox();
			this.btnPostLogin = new global::System.Windows.Forms.Button();
			base.SuspendLayout();
			this.Label1.AutoSize = true;
			this.Label1.Font = new global::System.Drawing.Font("Microsoft Sans Serif", 15f, global::System.Drawing.FontStyle.Bold, global::System.Drawing.GraphicsUnit.Point, global::System.Convert.ToByte(0));
			this.Label1.Location = new global::System.Drawing.Point(85, 9);
			this.Label1.Name = "Label1";
			this.Label1.Size = new global::System.Drawing.Size(161, 25);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Authentification";
			this.lblLogin.AutoSize = true;
			this.lblLogin.Location = new global::System.Drawing.Point(16, 60);
			this.lblLogin.Name = "lblLogin";
			this.lblLogin.Size = new global::System.Drawing.Size(59, 13);
			this.lblLogin.TabIndex = 1;
			this.lblLogin.Text = "Identifiant :";
			this.txtLogin.Location = new global::System.Drawing.Point(123, 53);
			this.txtLogin.Name = "txtLogin";
			this.txtLogin.Size = new global::System.Drawing.Size(187, 20);
			this.txtLogin.TabIndex = 2;
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new global::System.Drawing.Point(16, 91);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new global::System.Drawing.Size(77, 13);
			this.lblPassword.TabIndex = 3;
			this.lblPassword.Text = "Mot de passe :";
			this.txtPassword.Location = new global::System.Drawing.Point(123, 83);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new global::System.Drawing.Size(187, 20);
			this.txtPassword.TabIndex = 4;
			this.txtPassword.UseSystemPasswordChar = true;
			this.btnPostLogin.Location = new global::System.Drawing.Point(101, 183);
			this.btnPostLogin.Name = "btnPostLogin";
			this.btnPostLogin.Size = new global::System.Drawing.Size(134, 25);
			this.btnPostLogin.TabIndex = 5;
			this.btnPostLogin.Text = "Connexion";
			this.btnPostLogin.UseVisualStyleBackColor = true;
            this.btnPostLogin.Click += new System.EventHandler(this.btnPostLogin_Click);
			base.AutoScaleDimensions = new global::System.Drawing.SizeF(6f, 13f);
			base.AutoScaleMode = global::System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new global::System.Drawing.Size(322, 261);
			base.Controls.Add(this.btnPostLogin);
			base.Controls.Add(this.txtPassword);
			base.Controls.Add(this.lblPassword);
			base.Controls.Add(this.txtLogin);
			base.Controls.Add(this.lblLogin);
			base.Controls.Add(this.Label1);
			base.FormBorderStyle = global::System.Windows.Forms.FormBorderStyle.FixedSingle;
			base.MaximizeBox = false;
			base.Name = "frmLogin";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		// Token: 0x04000003 RID: 3
		private global::System.ComponentModel.IContainer components;

		// Token: 0x04000004 RID: 4
		internal global::System.Windows.Forms.Label Label1;

		// Token: 0x04000005 RID: 5
		internal global::System.Windows.Forms.Label lblLogin;

		// Token: 0x04000006 RID: 6
		internal global::System.Windows.Forms.TextBox txtLogin;

		// Token: 0x04000007 RID: 7
		internal global::System.Windows.Forms.Label lblPassword;

		// Token: 0x04000008 RID: 8
		internal global::System.Windows.Forms.TextBox txtPassword;

        internal global::System.Windows.Forms.Button btnPostLogin;
	}
}
