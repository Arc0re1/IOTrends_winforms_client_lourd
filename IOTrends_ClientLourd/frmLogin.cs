﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;
using IOTrends_ClientLourd.Models;

namespace IOTrends_ClientLourd
{
	// Token: 0x02000003 RID: 3
	[DesignerGenerated]
	public partial class frmLogin : Form
	{
		// Token: 0x06000003 RID: 3 RVA: 0x000020C4 File Offset: 0x000002C4
		private void btnPostLogin_Click(object sender, EventArgs e)
		{
			bool flag = string.IsNullOrWhiteSpace(this.txtLogin.Text) | string.IsNullOrWhiteSpace(this.txtPassword.Text);
			if (flag)
			{
				Interaction.MsgBox("Merci de bien remplir les champs afin de vous connecter.", MsgBoxStyle.OkOnly, Strings.Title_ErrorString);
			}
			string res = HttpHelper.GetData("?CheckUserCredentials=1&UserName=" + this.txtLogin.Text + "&UserPassword=" + this.txtPassword.Text);
			string okJSON = res.Substring(1, res.Length - 2);
            try
            {
                User newUser = JsonConvert.DeserializeObject<User>(okJSON);
                bool flag2 = newUser != null;
                if (flag2)
                {
                    base.Hide();
                    new MainForm
                    {
                        CurrentUser = newUser
                    }.Show();
                }
            }
            catch (Newtonsoft.Json.JsonReaderException)
            {
                MessageBox.Show("Cet utilisateur n'existe pas. (TODO: Gerer la creation d'User)", Strings.Popup_Login_Failed);
            }
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002177 File Offset: 0x00000377
		private void frmLogin_Load(object sender, EventArgs e)
		{
			this.Text = Strings.Title_LoginForm;
			base.CenterToScreen();
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000007 RID: 7 RVA: 0x0000256C File Offset: 0x0000076C
		// (set) Token: 0x06000008 RID: 8 RVA: 0x00002584 File Offset: 0x00000784
		//internal Button btnPostLogin
		//{
		//	get
		//	{
		//		return this.withEventsField_btnPostLogin;
		//	}
		//	set
		//	{
		//		bool flag = this.withEventsField_btnPostLogin != null;
		//		if (flag)
		//		{
		//			this.withEventsField_btnPostLogin.Click -= new EventHandler(this.btnPostLogin_Click);
		//		}
		//		this.withEventsField_btnPostLogin = value;
		//		bool flag2 = this.withEventsField_btnPostLogin != null;
		//		if (flag2)
		//		{
		//			this.withEventsField_btnPostLogin.Click += new EventHandler(this.btnPostLogin_Click);
		//		}
		//	}
		//}

		// Token: 0x06000009 RID: 9 RVA: 0x000025E7 File Offset: 0x000007E7
		public frmLogin()
		{
			base.Load += new EventHandler(this.frmLogin_Load);
			this.InitializeComponent();
		}

		// Token: 0x04000009 RID: 9
		//private Button withEventsField_btnPostLogin;
	}
}
