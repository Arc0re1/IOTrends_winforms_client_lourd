﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using IOTrends_ClientLourd.My;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using IOTrends_ClientLourd.Models;
using System.Collections.Generic;

namespace IOTrends_ClientLourd
{
	// Token: 0x02000004 RID: 4
	[DesignerGenerated]
	public partial class MainForm : Form
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000A RID: 10 RVA: 0x0000260B File Offset: 0x0000080B
		// (set) Token: 0x0600000B RID: 11 RVA: 0x00002613 File Offset: 0x00000813
		public User CurrentUser
		{
			get;
			set;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x0000261C File Offset: 0x0000081C
		private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			MyProject.Forms.frmLogin.Close();
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002630 File Offset: 0x00000830
		private void MainForm_Load(object sender, EventArgs e)
		{
			this.Text = Strings.Title_MainForm;
			base.CenterToScreen();
			bool flag = this.CurrentUser != null;
			if (flag)
			{
				this.txtAdmName.Text = this.CurrentUser.Nom;
				this.txtAdmFirstName.Text = this.CurrentUser.Prenom;
				this.txtAdmPassword.Text = this.CurrentUser.Password;
				this.txtAdmDatabaseID.Text = this.CurrentUser.IdUtilisateur;
				this.txtAdmRights.Text = this.CurrentUser.Droits;
                this.RefreshGrid();
			}
			else
			{
                this.ErrorAndCrash();
			}
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002FB9 File Offset: 0x000011B9
		public MainForm()
		{
			base.Load += new EventHandler(this.MainForm_Load);
			base.FormClosed += new FormClosedEventHandler(this.MainForm_FormClosed);
			this.InitializeComponent();
		}

        private void ErrorAndCrash()
        {
            Interaction.MsgBox("Une erreur s'est produite. Le client va maintenant crasher.", MsgBoxStyle.OkOnly, Strings.Title_ErrorString);
        }

        private void RefreshGrid()
        {
            if (this.CurrentUser != null)
            {
                string url = "?GetLocksList=1&UserRightsID=" + this.CurrentUser.IdDroits;
                // Ne s'affiche pas ?
                this.lblVerrRefreshLoading.Text = Strings.Verr_RefreshLoadingMessage;
                string resultJSON = HttpHelper.GetData(url);
                List<Lock> locks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Lock>>(resultJSON);
                BindingList<Lock> bindedList = new BindingList<Lock>(locks);
                this.grdLocksList.DataSource = bindedList;
                this.grdLocksList.Columns["Label"].HeaderText = Strings.Verr_Grid_label;
                this.grdLocksList.Columns["Rights"].HeaderText = Strings.Verr_Grid_rights;
                this.grdLocksList.Columns["LabelRights"].HeaderText = Strings.Verr_Grid_rightsLabel;
                this.lblVerrRefreshLoading.Text = string.Empty;
                this.grdLocksList.RowHeaderMouseDoubleClick += this.grdLocksList_RowHeaderMouseDoubleClick;
                this.grdLocksList.MouseClick += this.grdLocksList_MouseClick;
            }
            else
            {
                this.ErrorAndCrash();
            }
        }

        private void btnVerrRefresh_Click(object sender, EventArgs e)
        {
            this.RefreshGrid();
        }

        private void grdLocksList_RowHeaderMouseDoubleClick(object sender, EventArgs e)
        {
            var selectedRow = this.grdLocksList.SelectedRows[0];
            frmLocks_EditRow editForm = new frmLocks_EditRow(ref this.grdLocksList, selectedRow);
            editForm.FormClosing += this.frmLocks_EditRow_FormClosing;
            editForm.Show();
        }

        private void frmLocks_EditRow_FormClosing(object sender, EventArgs e)
        {
            this.RefreshGrid();
        }

        private void btnAddNewLock_Click(object sender, EventArgs e)
        {
            frmLocks_AddRow addForm = new frmLocks_AddRow(ref this.grdLocksList);
            addForm.FormClosing += this.frmLocks_EditRow_FormClosing;
            addForm.Show();
        }

        private void grdLocksList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int currentMouseOverRow = this.grdLocksList.HitTest(e.X, e.Y).RowIndex;

                if (currentMouseOverRow >= 0)
                {
                    ContextMenu contextMenu = new ContextMenu();
                    contextMenu.MenuItems.Add(new MenuItem(Strings.Verr_Grid_rightClickMenu_delete, this.grdLocksList_Delete));
                    MainForm.sCurrentMouseOverRow = currentMouseOverRow;
                    contextMenu.Show(this.grdLocksList, new Point(e.X, e.Y));
                }
            }
        }

        private void grdLocksList_Delete(object sender, EventArgs e)
        {
            if (MainForm.sCurrentMouseOverRow >= 0)
            {
                DataGridViewRow rowToDelete = this.grdLocksList.Rows[sCurrentMouseOverRow];
                string message = string.Format("Verrou numéro : {0}, libéllé : {1}, appartenant à : {2}", rowToDelete.Cells["Id"].Value, rowToDelete.Cells["Label"].Value, rowToDelete.Cells["LabelRights"].Value);
                string caption = Strings.Popup_Grid_rightClickMenu_delete;
                DialogResult result = MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.OK)
                {
                    Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                    json["RequestName"] = "DeleteLock";
                    json["LockID"] = rowToDelete.Cells["Id"].Value.ToString();
                    HttpHelper.PostData(json.ToString());
                    this.RefreshGrid();
                }
            }
        }

        private void btnInfoAdmModify_Click(object sender, EventArgs e)
        {
            frmAdm_EditUser editUser = new frmAdm_EditUser(this.txtAdmDatabaseID.Text);
            editUser.FormClosing += this.btnAdmReload_Click;
            editUser.Show();
        }

        private void btnAdmReload_Click(object sender, EventArgs e)
        {
            this.ReloadUser();
        }

        private void ReloadUser()
        {
            string userdata = HttpHelper.GetData("?GetUserData=1&UserID=" + this.CurrentUser.IdUtilisateur);
            User newUser = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(userdata);
            if (newUser != null)
            {
                this.CurrentUser = newUser;
            }
            this.txtAdmName.Text = this.CurrentUser.Nom;
            this.txtAdmFirstName.Text = this.CurrentUser.Prenom;
            this.txtAdmPassword.Text = this.CurrentUser.Password;
            this.txtAdmDatabaseID.Text = this.CurrentUser.IdUtilisateur;
            this.txtAdmRights.Text = this.CurrentUser.Droits;
        }

        private static int sCurrentMouseOverRow;
    }
}
